#!/bin/bash

#------------------------------------------------------------------------------
# Copyright (C) 2018 Jesus Ortiz
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#------------------------------------------------------------------------------

###############################################################################
# COLORS
###############################################################################

# Function to reset the terminal colors
function color_reset
{
  echo -ne "\033[0m"
}

# List of usefull colors
COLOR_RESET="\033[0m"
COLOR_INFO="\033[0;32m"
COLOR_ITEM="\033[1;34m"
COLOR_QUES="\033[1;32m"
COLOR_WARN="\033[0;33m"
COLOR_CODE="\033[0m"
COLOR_BOLD="\033[1m"
COLOR_UNDE="\033[4m"


###############################################################################
# FUNCTIONS
###############################################################################

# Arguments:
#  $1: string
#  $2: file
function extract_file
{
  C_BEG=$(grep -c "<<<$1>>>" $2)
  C_END=$(grep -c ">>>$1<<<" $2)

  if [[ $C_BEG -ne "1" || $C_END -ne "1" ]]
  then
    echo "String not found"
    return
  fi

  N_BEG=$(grep -n "<<<$1>>>" $2 | awk -F':' '{ print $1 }')
  N_END=$(grep -n ">>>$1<<<" $2 | awk -F':' '{ print $1 }')
  N_END=$(( $N_END - 1 ))
  N_SIZE=$(( $N_END - $N_BEG ))

  head -n $N_END $2 | tail -n $N_SIZE > $1
}

function create_folders
{
  echo "Creating project folders..."
  mkdir app
  mkdir lib
  mkdir etc
  mkdir etc/cmake
  mkdir etc/scripts
  mkdir etc/doc
}

# Arguments:
#  $1: this file
function create_readme_files
{
  echo "Creating README files..."

  extract_file README.md $1
  sed -i -e 's/$PROJECT_NAME/'"${PROJECT_NAME}"'/g' README.md
  sed -i -e 's/$PROJECT_TITLE/'"${PROJECT_TITLE}"'/g' README.md
  sed -i -e 's/$PROJECT_DESCRIPTION/'"${PROJECT_DESCRIPTION}"'/g' README.md

  extract_file app/README.md $1
  extract_file lib/README.md $1
  extract_file etc/README.md $1
  extract_file etc/cmake/README.md $1
  extract_file etc/scripts/README.md $1
}

# Arguments:
#  $1: this file
function create_cmake_files
{
  echo "Creating CMake files..."

  extract_file CMakeLists.txt $1
  sed -i -e 's/$PROJECT_NAME/'"${PROJECT_NAME}"'/g' CMakeLists.txt

  extract_file app/CMakeLists.txt $1
  extract_file lib/CMakeLists.txt $1
}

# Arguments:
#  $1: this file
function create_script_files
{
  echo "Creating script files..."
  extract_file etc/scripts/colors.sh $1
  chmod +x etc/scripts/colors.sh
  extract_file etc/scripts/configure.sh $1
  chmod +x etc/scripts/configure.sh
}

function add_app
{
  echo "Creating app..."
  
  echo "  Creating app folders..."
  mkdir app/$2
  mkdir app/$2/include
  mkdir app/$2/src
  
  echo "  Adding app path to CMake files..."
  echo "add_subdirectory($2)" >> app/CMakeLists.txt

  echo "  Creating CMake files..."
  extract_file app/sample.txt $1
  mv app/sample.txt app/$2/CMakeLists.txt
  sed -i -e 's/$APP_NAME/'"${2}"'/g' app/$2/CMakeLists.txt

  echo "  Creating main.cpp..."
  extract_file app/main.cpp $1
  mv app/main.cpp app/$2/src/main.cpp
  sed -i -e 's/$APP_NAME/'"${2}"'/g' app/$2/src/main.cpp
  sed -i -e 's/$AUTHOR/'"${AUTHOR}"'/g' app/$2/src/main.cpp
  sed -i -e 's/$DATE/'"${DATE}"'/g' app/$2/src/main.cpp
}

function add_library
{
  echo "Creating library..."
  
  echo "  Creating library folders..."
  mkdir lib/$2
  mkdir lib/$2/include
  mkdir lib/$2/src
  mkdir lib/$2/test
  
  echo "  Adding library path to CMake files..."
  echo "add_subdirectory($2)" >> lib/CMakeLists.txt

  echo "  Creating CMake files..."
  extract_file lib/sample.txt $1
  mv lib/sample.txt lib/$2/CMakeLists.txt
  sed -i -e 's/$LIB_NAME/'"${2}"'/g' lib/$2/CMakeLists.txt

  echo "  Creating $2.h..."
  extract_file lib/lib.h $1
  mv lib/lib.h lib/$2/include/$2.h
  sed -i -e 's/$LIB_NAME_CAPS/'"${2^^}"'/g' lib/$2/include/$2.h
  sed -i -e 's/$LIB_NAME/'"${2}"'/g' lib/$2/include/$2.h
  sed -i -e 's/$AUTHOR/'"${AUTHOR}"'/g' lib/$2/include/$2.h
  sed -i -e 's/$DATE/'"${DATE}"'/g' lib/$2/include/$2.h

  echo "  Creating $2.cpp..."
  extract_file lib/lib.cpp $1
  mv lib/lib.cpp lib/$2/src/$2.cpp
  sed -i -e 's/$LIB_NAME/'"${2}"'/g' lib/$2/src/$2.cpp
  sed -i -e 's/$AUTHOR/'"${AUTHOR}"'/g' lib/$2/src/$2.cpp
  sed -i -e 's/$DATE/'"${DATE}"'/g' lib/$2/src/$2.cpp
}

# Function to display the help
function display_help
{
  color_reset
  echo -e "${COLOR_BOLD}NAME${COLOR_RESET}"
  echo -e "       pgen.sh - Script to generate a project structure with CMake"
  echo -e ""
  echo -e "${COLOR_BOLD}SYNOPSIS${COLOR_RESET}"
  echo -e "       ${COLOR_BOLD}pgen.sh${COLOR_RESET} [${COLOR_UNDE}OPTIONS${COLOR_RESET}]..."
  echo -e ""
  echo -e "${COLOR_BOLD}DESCRIPTION${COLOR_RESET}"
  echo -e "       This script prepares a project structure for a C/C++ project using CMake."
  echo -e ""
  echo -e "       You can add the following options when calling the script:"
  echo -e ""
  echo -e "       ${COLOR_BOLD}-h${COLOR_RESET}, ${COLOR_BOLD}--help${COLOR_RESET}"
  echo -e "              Display this help message"
  echo -e ""
  echo -e "       ${COLOR_BOLD}-g${COLOR_RESET}, ${COLOR_BOLD}--generate${COLOR_RESET}"
  echo -e "              Generates basic project structure."
  echo -e ""
  echo -e "       ${COLOR_BOLD}-l${COLOR_RESET}, ${COLOR_BOLD}--library${COLOR_RESET}"
  echo -e "              Adds a new library to an existing project."
  echo -e ""
  echo -e "       ${COLOR_BOLD}-a${COLOR_RESET}, ${COLOR_BOLD}--app${COLOR_RESET}"
  echo -e "              Adds a new application to an existing project."
  echo -e ""
  echo -e "       ${COLOR_BOLD}-c${COLOR_RESET}, ${COLOR_BOLD}--config${COLOR_RESET}"
  echo -e "              Use a configuration file."
  echo -e ""
  echo -e "${COLOR_BOLD}AUTHOR${COLOR_RESET}"
  echo -e "       Written by Jesus Ortiz."
  echo -e ""
  echo -e "${COLOR_BOLD}REPORTING BUGS${COLOR_RESET}"
  echo -e "       Report bugs to <jortizsl@gmail.com>"
  echo -e ""
  echo -e "${COLOR_BOLD}COPYRIGHT${COLOR_RESET}"
  echo -e "       Copyright (C) 2018 Jesus Ortiz"
  echo -e ""     
  echo -e "       This program is free software: you can redistribute it and/or modify"
  echo -e "       it under the terms of the GNU General Public License as published by"
  echo -e "       the Free Software Foundation, either version 3 of the License, or"
  echo -e "       (at your option) any later version."
  echo -e ""
  echo -e "       This program is distributed in the hope that it will be useful,"
  echo -e "       but WITHOUT ANY WARRANTY; without even the implied warranty of"
  echo -e "       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
  echo -e "       GNU General Public License for more details."
  echo -e ""
  echo -e "       You should have received a copy of the GNU General Public License"
  echo -e "       along with this program. If not, see <http://www.gnu.org/licenses/>."
  echo -e ""
  color_reset
}

# Function to display new project menu
function display_generate
{
  echo "Welcome to pgen, a script to generate a base c++ project structure."
  echo ""
  echo "Please, answer the following questions in order to configure your new project."
  echo ""
  echo -n "  Project title [short string to identify the project]: "
  read PROJECT_TITLE
  echo -n "  Project name [single word to identify the project for file system names]: "
  read PROJECT_NAME
  echo -n "  Project description [short sentence to describe the project]: "
  read PROJECT_DESCRIPTION
  echo -n "  Author's full name: "
  read AUTHOR
  echo -n "  Author's email: "
  read EMAIL

  create_folders
  create_readme_files $1
  create_cmake_files $1
  create_script_files $1
}

# Function to display add library menu
function display_lib
{
  echo "Welcome to pgen, a script to generate a base c++ project structure."
  echo ""
  echo "Please, answer the following questions in order to add a new library."
  echo ""
  echo -n "  Library name [single word to identify the library]: "
  read LIBRARY_NAME
  echo -n "  Author's full name: "
  read AUTHOR

  add_library $1 $LIBRARY_NAME
}

# Function to display add application menu
function display_app
{
  echo "Welcome to pgen, a script to generate a base c++ project structure."
  echo ""
  echo "Please, answer the following questions in order to add a new application."
  echo ""
  echo -n "  Application name [single word to identify the application]: "
  read APPLICATION_NAME
  echo -n "  Author's full name: "
  read AUTHOR

  add_app $1 $APPLICATION_NAME
}


###############################################################################
# VARIABLES
###############################################################################

PROJECT_TITLE="Test"
PROJECT_DESCRIPTION="This is a test program"
PROJECT_NAME="test"

AUTHOR="Author"
EMAIL="author@xxx.com"
DATE=$(date +%d-%b-%Y)


###############################################################################
# ARGUMENTS
###############################################################################

# Check arguments
if [ $# -eq 0 ]; then
  display_help
  exit
fi

for ARG in "$@"; do
  if   [ $ARG == "-h" ] || [ $ARG == "--help" ]; then      # Display help
    display_help
    exit
  elif [ $ARG == "-g" ] || [ $ARG == "--generate" ]; then  # Creates a new project structure
    echo "Generating new project structure"
    display_generate $0
  elif [ $ARG == "-l" ] || [ $ARG == "--library" ]; then   # Adds a new library to an existing project
    echo "Adding library"
    display_lib $0
  elif [ $ARG == "-a" ] || [ $ARG == "--app" ]; then       # Adds a new application to an existing project
    echo "Adding application"
    display_app $0
  elif [ $ARG == "-c" ] || [ $ARG == "--config" ]; then    # Use a configuration file
    echo "Loading configuration file"
    # TODO
  else
    echo "Unrecognized parameter '$ARG'. Please read the documentation with the --help parameter"
    exit
  fi
done

exit


###############################################################################
# END OF SCRIPT CODE
###############################################################################

exit


###############################################################################
# README.md files
###############################################################################

<<<README.md>>>
$PROJECT_TITLE
=======

$PROJECT_DESCRIPTION


File structure
==============

The folders of the project are the following:

 - app     -> Folder containing source code of applications
 - etc     -> Folder containing non source code files
   - cmake   -> CMake files (find files and other auxiliary files)
   - scripts -> Folder containing scripts
 - lib     -> Folder containing source code of libraries
 
In the root folder of the project it should be a CMakeLists.txt file to
compile the project with CMake.


Compilation
===========

These are the steps to compile $PROJECT_TITLE (using only CMake):

    $ cd path/to/$PROJECT_NAME/source
    $PROJECT_NAME$ mkdir build
    $PROJECT_NAME$ cd build
    $PROJECT_NAME/build$ cmake ..
    $PROJECT_NAME/build$ make
    $PROJECT_NAME/build$ make install      (the default folder is a local folder)

To compile in debug mode run CMake with the following arguments:

    $PROJECT_NAME/build$ cmake .. -DCMAKE_BUILD_TYPE=Debug

To compile in release mode run CMake with the following arguments:

    $PROJECT_NAME/build$ cmake .. -DCMAKE_BUILD_TYPE=Release

These are the steps to compile $PROJECT_TITLE (using configure.sh script):

    $ cd path/to/$PROJECT_NAME/source
    $PROJECT_NAME$ mkdir build
    $PROJECT_NAME$ cd build
    $PROJECT_NAME/build$ ../scripts/configure.sh    (see instructions below)
    $PROJECT_NAME/build$ make
    $PROJECT_NAME/build$ make install      (the default folder is a local folder)

The configure script accepts several parameters:

 - (-h), (--help). Displays a help message
 - (-g), (--debug). Compiles in debug mode
 - (-r), (--release). Compiles in release mode
 - (--reset). Removes all the contents of the folder before calling CMake
 - (-DCMAKEVARIABLE). Calls CMake with some variable

A typical call to the configuration script would be:

    $PROJECT_NAME/build$ ../scripts/configure.sh --reset -r


Installation
============

To install the programs you can use the following command:

    $PROJECT_NAME/build$ make install
>>>README.md<<<

#------------------------------------------------------------------------------

<<<app/README.md>>>
Folder to put the applications.

Each project has at least two folders:

 - include -> Include files
 - src     -> Source files
 
In the root folder of the project it should be a CMakeLists.txt file to
compile the project with CMake.
>>>app/README.md<<<

#------------------------------------------------------------------------------

<<<lib/README.md>>>
Folder to put the library projects.

Each project has at least three folders:

 - include -> Include files
 - src     -> Source files
 - test    -> Test files
 
In the root folder of the project it should be a CMakeLists.txt file to
compile the project with CMake.
>>>lib/README.md<<<

#------------------------------------------------------------------------------

<<<etc/README.md>>>
Folder to put other files.
>>>etc/README.md<<<

#------------------------------------------------------------------------------

<<<etc/cmake/README.md>>>
Folder to put CMake files.
>>>etc/cmake/README.md<<<

#------------------------------------------------------------------------------

<<<etc/scripts/README.md>>>
Folder to put scripts.
>>>etc/scripts/README.md<<<


###############################################################################
# CMake files
###############################################################################

<<<CMakeLists.txt>>>
# Project configuration
cmake_minimum_required(VERSION 2.8)
project($PROJECT_NAME)
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/etc/cmake)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})


# Compiler options
set(CMAKE_CXX_FLAGS "-Wall -pedantic")
set(CMAKE_EXE_LINKER_FLAGS "-Wall -pedantic")

set(CMAKE_CXX_FLAGS_RELEASE "-O3")
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-O3")

set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_EXE_LINKER_FLAGS_DEBUG "-g")


# Set Doxygen
set(DOXYFILE_OUTPUT_DIR "etc/doc")
set(DOXYFILE_LATEX "OFF")
include(UseDoxygen OPTIONAL)

if (DOXYGEN_FOUND)
  message("DOXYGEN found")
else (DOXYGEN_FOUND)
  message("DOXYGEN not found")
endif (DOXYGEN_FOUND)


# Set library
#find_package()
#
#if (_FOUND)
#  message(" found")
#else (_FOUND)
#  message(" not found")
#endif (_FOUND)


# Install path
set(INSTALL_PREFIX ${PROJECT_SOURCE_DIR}/install CACHE PATH "Install prefix")
set(INSTALL_BIN_PATH ${INSTALL_PREFIX}/bin)
set(INSTALL_LIB_PATH ${INSTALL_PREFIX}/lib)
set(INSTALL_INC_PATH ${INSTALL_PREFIX}/include)
set(INSTALL_TEST_PATH ${INSTALL_PREFIX}/test)
install(DIRECTORY ${PROJECT_SOURCE_DIR}/etc/cmake DESTINATION ${INSTALL_PREFIX})


# Enable tests
enable_testing()


# Set subdirectories
add_subdirectory(lib)
add_subdirectory(app)
>>>CMakeLists.txt<<<

#------------------------------------------------------------------------------

<<<app/CMakeLists.txt>>>
# Add the application subdirectories
>>>app/CMakeLists.txt<<<

#------------------------------------------------------------------------------

<<<app/sample.txt>>>
# Project configuration
project($APP_NAME)

# Include directories
#include_directories()

# Add executable
add_executable($APP_NAME
  src/main.cpp)
#target_link_libraries($APP_NAME ...)
install(TARGETS $APP_NAME
  RUNTIME DESTINATION ${INSTALL_BIN_PATH})
>>>app/sample.txt<<<

#------------------------------------------------------------------------------

<<<lib/CMakeLists.txt>>>
# Add the library subdirectories
>>>lib/CMakeLists.txt<<<

#------------------------------------------------------------------------------

<<<lib/sample.txt>>>
# Project configuration
project($LIB_NAME)

# Include directories
include_directories(include)

# Install include files
file(GLOB HEADERS "include/*.h")
install(FILES ${HEADERS} DESTINATION ${INSTALL_INC_PATH})


# Add library
add_library($LIB_NAME
  src/$LIB_NAME.cpp)
  
# Install library
install(TARGETS $LIB_NAME
  ARCHIVE DESTINATION ${INSTALL_LIB_PATH})

  
# Add test
#add_executable(test_
#  test/test_.cpp)
#target_link_libraries(test_
#  $LIB_NAME)
#add_test(
#  ${EXECUTABLE_OUTPUT_PATH}/test_)
>>>lib/sample.txt<<<


###############################################################################
# Scripts
###############################################################################

<<<etc/scripts/colors.sh>>>
#!/bin/bash

#------------------------------------------------------------------------------
#
# Script to setup terminal colors
#
# Advanced Robotics Department
# Fondazione Istituto Italiano di Tecnologia
#
# Author: Jesus Ortiz
# email : jesus.ortiz@iit.it
# Date  : 02-Nov-2012
#
#------------------------------------------------------------------------------

# Function to reset the terminal colors
function color_reset
{
  echo -ne "\033[0m"
}

# List of usefull colors
COLOR_RESET="\033[0m"
COLOR_INFO="\033[0;32m"
COLOR_ITEM="\033[1;34m"
COLOR_QUES="\033[1;32m"
COLOR_WARN="\033[0;33m"
COLOR_CODE="\033[0m"
COLOR_BOLD="\033[1m"
COLOR_UNDE="\033[4m"

# To use the colors use the command "echo -e" or "echo -ne" (without newline)
# and put the label before the text in the way ${COLOR_XXXX}
>>>etc/scripts/colors.sh<<<

#------------------------------------------------------------------------------

<<<etc/scripts/configure.sh>>>
#!/bin/bash

#------------------------------------------------------------------------------
#
# Script to configure the building folder for CMake
#
# Advanced Robotics Department
# Fondazione Istituto Italiano di Tecnologia
#
# Author: Jesus Ortiz
# email : jesus.ortiz@iit.it
# Date  : 13-Sept-2016
#
#------------------------------------------------------------------------------

# Set color script
SCRIPT_FOLDER=$(dirname $0)
source $SCRIPT_FOLDER/colors.sh


# Function to display the help
function display_help
{
  color_reset
  echo -e "${COLOR_BOLD}NAME${COLOR_RESET}"
  echo -e "       configure.sh - Script to configure the building folder for CMake"
  echo -e ""
  echo -e "${COLOR_BOLD}SYNOPSIS${COLOR_RESET}"
  echo -e "       ${COLOR_BOLD}configure.sh${COLOR_RESET} [${COLOR_UNDE}OPTIONS${COLOR_RESET}]..."
  echo -e ""
  echo -e "${COLOR_BOLD}DESCRIPTION${COLOR_RESET}"
  echo -e "       This script configures the building folder for CMake. The script has to"
  echo -e "       be called from the build folder."
  echo -e ""
  echo -e "       You can add the following options when calling the script:"
  echo -e ""
  echo -e "       ${COLOR_BOLD}-h${COLOR_RESET}, ${COLOR_BOLD}--help${COLOR_RESET}"
  echo -e "              Display this help message"
  echo -e ""
  echo -e "       ${COLOR_BOLD}-g${COLOR_RESET}, ${COLOR_BOLD}--debug${COLOR_RESET}"
  echo -e "              Compiles in debug mode."
  echo -e "              If not compiling mode is set, CMake is called without flags."
  echo -e ""
  echo -e "       ${COLOR_BOLD}-r${COLOR_RESET}, ${COLOR_BOLD}--release${COLOR_RESET}"
  echo -e "              Compiles in release mode"
  echo -e "              If not compiling mode is set, CMake is called without flags."
  echo -e ""
  echo -e "       ${COLOR_BOLD}-D${COLOR_RESET}${COLOR_UNDE}CMAKEVARIABLE${COLOR_RESET}"
  echo -e "              Specifies a CMake option. CMake will be invoked with the argument"
  echo -e "              -DCMAKEVARIABLE"
  echo -e ""
  echo -e "       ${COLOR_BOLD}--reset${COLOR_RESET}"
  echo -e "              Deletes everything in the folder before configuring it"
  echo -e ""
  echo -e "${COLOR_BOLD}AUTHOR${COLOR_RESET}"
  echo -e "       Written by Jesus Ortiz."
  echo -e ""
  echo -e "${COLOR_BOLD}REPORTING BUGS${COLOR_RESET}"
  echo -e "       Report bugs to <jesus.ortiz@iit.it>"
  echo -e ""
  echo -e "${COLOR_BOLD}COPYRIGHT${COLOR_RESET}"
  echo -e "       This software is not for public distribution."
  echo -e ""
  color_reset
}


# Calculate the root folder
ROOT_DIR=$(pwd)/$(dirname $0)/../..

# Clean root path
OLDIFS="$IFS"
IFS='/'
END=false
while ! $END; do
  FOUND_BACK=false
  AUX=""
  ROOT_DIR_AUX=""
  for TOKEN in $ROOT_DIR; do
    if $FOUND_BACK; then
      ROOT_DIR_AUX=${ROOT_DIR_AUX}/${TOKEN}
    else
      if [ "$TOKEN" == ".." ]; then
        FOUND_BACK=true
      else
        if [ -n "$ROOT_DIR_AUX" ]; then
          ROOT_DIR_AUX=${ROOT_DIR_AUX}/${AUX}
        else
          ROOT_DIR_AUX=${AUX}
        fi
        AUX=$TOKEN
      fi
    fi
  done
  if ! $FOUND_BACK; then
    END=true
  else
    ROOT_DIR="/${ROOT_DIR_AUX}"
  fi
done
IFS="$OLDIFS"


# Reset terminal color
color_reset
echo ""


# Check arguments
for ARG in "$@"; do
  if   [ $ARG == "-h" ] || [ $ARG == "--help" ]; then     # Display help
    display_help
    exit
  elif [ $ARG == "-g" ] || [ $ARG == "--debug" ]; then    # Compiles with the debug flag
    echo -e "${COLOR_INFO} + Debug mode${COLOR_RESET}"
    OPTIONS="$OPTIONS -DCMAKE_BUILD_TYPE=Debug"
  elif [ $ARG == "-r" ] || [ $ARG == "--release" ]; then  # Compiles with the release flag
    echo -e "${COLOR_INFO} + Release mode${COLOR_RESET}"
    OPTIONS="$OPTIONS -DCMAKE_BUILD_TYPE=Release"
  elif [ ${ARG:0:2} == "-D" ]; then                       # cmake definitions
    OPTIONS="$OPTIONS -D${ARG:2}"
  elif [ $ARG == "--reset" ]; then                        # Cleans the current folder before configuration
    echo -e "${COLOR_INFO} + Cleaning folder...${COLOR_RESET}"
    rm -rf *
  else
    echo "Unrecognized parameter '$ARG'. Please read the documentation with the --help parameter"
    exit
  fi
done


# Launch cmake with the automatic options
echo -e "${COLOR_INFO} + Launching CMake...${COLOR_RESET}"
echo -e "cmake ${ROOT_DIR} $OPTIONS"
echo ""
color_reset
cmake ${ROOT_DIR} $OPTIONS
>>>etc/scripts/configure.sh<<<


###############################################################################
# Source files
###############################################################################

<<<app/main.cpp>>>
/**
 * @file	app/$APP_NAME/src/main.cpp
 * @author	$AUTHOR
 * @version	1.0
 * @date	$DATE
 * @brief	Write here app description
 */

#include <iostream>

int main(int argc, char *argv[])
{
	std::cout << "Hello, World!" << std::endl;
	return 0;
}
>>>app/main.cpp<<<

#------------------------------------------------------------------------------

<<<lib/lib.h>>>
/**
 * @file	$LIB_NAME.h
 * @author	$AUTHOR
 * @version	1.0
 * @date	$DATE
 * @brief	Write here library description
 */

#ifndef $LIB_NAME_CAPS_H_
#define $LIB_NAME_CAPS_H_



#endif	/* $LIB_NAME_CAPS_H_ */
>>>lib/lib.h<<<

#------------------------------------------------------------------------------

<<<lib/lib.cpp>>>
/**
 * @file	$LIB_NAME.cpp
 * @author	$AUTHOR
 * @version	1.0
 * @date	$DATE
 * @brief	Write here library description
 */

/*
 * INCLUDES
 */

#include "$LIB_NAME.h"

>>>lib/lib.cpp<<<

